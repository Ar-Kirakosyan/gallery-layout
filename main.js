
let items = [] ;
let inAdvance = 100;

function lazyLoad() {
  items.forEach((item) => {
    if (item.offsetTop < window.innerHeight + window.pageYOffset + inAdvance && !$(item).hasClass('loaded')) {
		$(item).addClass('loaded')

		var thumbs = $(item).find('.thumb-sub')
		var images = [...item.querySelectorAll("img")];
		images.forEach((image) => {
      	
			image.src = image.dataset.src;
	      	image.onload = function(){
		      	if(!$(this).hasClass('fadeInUp')){
		      		let curImg = $(this)
		      		curImg.addClass('loaded')
		      		setTimeout(function(){
				      	curImg.addClass("animated animatedFadeInUp fadeInUp");
		      		},200)

		      		var loaded = true;
			      	thumbs.each(function(){
			      		if(!$(this).find('img').hasClass('loaded')){
			      			loaded = false
			      		}
			      	})

			      	if(loaded == true){
			      		var default_ratio = 0;
						let item_height = 0;
						thumbs.each(function(){
							let width = $(this).find('img').get(0).naturalWidth
							let height = $(this).find('img').get(0).naturalHeight
							let flex = 1

							if(default_ratio == 0)
								default_ratio = height / width
							ratio = height / width
							flex = parseInt(Math.round((ratio / default_ratio) * 10))
							item_height += parseInt($(this).width() * ratio)
							$(this).attr('style','flex:'+flex)
						})

						if(item_height > 0)
							item.style = "height:" + item_height + 'px';
			      	}
		      	}

		      	
		    }
      	})
    }
  });
}

$(function(){
	items = [...document.querySelectorAll(".item")];
	$('.item').each(function(){
		width = $(this).width()
		$(this).attr('style','min-height:' + width + 'px')
	})
	lazyLoad();
})

function resize(){
	items.forEach((item) => {
		let item_height = 0;
		var thumbs = $(item).find('.thumb-sub')
		thumbs.each(function(){
			let width = $(this).find('img').get(0).naturalWidth
			let height = $(this).find('img').get(0).naturalHeight
			
			ratio = height / width
			item_height += parseInt($(this).width() * ratio)
		})
		if(item_height > 0)
			item.style = "height:" + item_height + 'px';
	});
}
document.addEventListener("scroll", lazyLoad);
window.addEventListener("resize", resize);
window.addEventListener("orientationchange", lazyLoad);
